﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelegramBotFit.Model.Command;

namespace TelegramBotFit.Model.Commands
{
    public class PythonCommand: BaseCommand
    {
        public override string AppRun { get; set; } = "python";
    }
}
