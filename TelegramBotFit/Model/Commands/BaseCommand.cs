﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBotFit.Model.Command
{
    public class BaseCommand
    {
        public virtual string AppRun { get; set; } = "sh";
        public virtual List<string> Args { get; set; } = new ();
    }
}
