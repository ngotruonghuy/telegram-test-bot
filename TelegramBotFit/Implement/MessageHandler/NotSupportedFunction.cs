﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramBotFit.Interface;

namespace TelegramBotFit.Implement.MessageHandler
{
    public class NotSupportedFunction : IMessageHandler
    {
        public async Task HandleMessagge(ITelegramBotClient botClient, Message message, CancellationToken cancellationToken)
        {
            var chatId = message?.Chat?.Id ?? -1;

            await botClient.SendTextMessageAsync(
               chatId: chatId,
               text: "Function is not supported!",
               cancellationToken: cancellationToken);
        }
    }
}
