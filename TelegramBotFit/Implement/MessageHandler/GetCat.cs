﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramBotFit.Interface;

namespace TelegramBotFit.Implement.MessageHandler
{
    public class GetCat : IMessageHandler
    {
        private readonly IHttpClient httpClient;

        public GetCat(IHttpClient? httpClient)
        {
            this.httpClient = httpClient ?? new HttpClientWrapper();
        }

        public GetCat(): this(null)
        { }

        public async Task HandleMessagge(ITelegramBotClient botClient, Message message, CancellationToken cancellationToken)
        {
            var file = await httpClient.DownloadFile(AppSetting.CatApi);
            
            await botClient.SendPhotoAsync(
                chatId: message.Chat.Id,
                photo: file,
                cancellationToken: cancellationToken);

        }
    }
}
