﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramBotFit.Interface;
using TelegramBotFit.Model.Commands;

namespace TelegramBotFit.Implement.MessageHandler
{
    public class ConvertToDiagramFunction : IMessageHandler
    {
        private readonly ICommandDispatcher dispatcher;

        public ConvertToDiagramFunction():this(null)
        { }

        public ConvertToDiagramFunction(ICommandDispatcher dispatcher)
        {
            this.dispatcher = dispatcher ?? new CommandDispatcher();
        }

        public async Task HandleMessagge(ITelegramBotClient botClient, Message message, CancellationToken cancellationToken)
        {
            var attachment = message.Document;

            var fileName = message?.Document?.FileName ?? "out.json";
            var fileExt = Path.GetExtension(fileName);
            fileName = fileName.Replace(fileExt, string.Empty);

            if (attachment is null)
            {
                await botClient.SendTextMessageAsync(
                    chatId: message.Chat.Id,
                    text: "Please attach file with /convert caption",
                    cancellationToken: cancellationToken);
                    
                return;
            }

            var workPath = Guid.NewGuid().ToString();
            FileHelper.CreateDirectory(workPath);
            var downloadFilePath = $"{workPath}/in{fileExt}";
            using Stream fileStream = System.IO.File.OpenWrite(downloadFilePath);

            await botClient.GetInfoAndDownloadFileAsync(
                fileId: attachment.FileId,
                destination: fileStream,
                cancellationToken: cancellationToken);

            fileStream.Close();

            var command = new PythonCommand
            {
                Args = new()
                {
                    AppSetting.ConvertAppDir,
                    $"-i {downloadFilePath}",
                    $"-o {workPath}/{fileName}"
                }
            };

            await dispatcher.SendAsync(command);

            try
            {
                using Stream result = System.IO.File.OpenRead($"{workPath}/{fileName}{AppSetting.ImageExt}");
                await botClient.SendPhotoAsync(
                   chatId: message.Chat.Id,
                   photo: result,
                   cancellationToken: cancellationToken);
                result.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                await botClient.SendTextMessageAsync(
                    chatId: message.Chat.Id,
                    text: "Have an error, please try again!");
            }
            finally {
                FileHelper.EmptyDirectory(workPath);
            }
        }
    }
}
