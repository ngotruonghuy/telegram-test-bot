﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelegramBotFit.Interface;
using Telegram.Bot.Types;

namespace TelegramBotFit.Implement
{
    public class HttpClientWrapper : IHttpClient, IDisposable
    {
        private readonly HttpClient _httpClient = new HttpClient(new HttpClientHandler
        {
            UseDefaultCredentials= true
        });

        private bool disposedValue;

        public async Task<Stream> DownloadFile(string destinationUrl)
        {
            var respone = await _httpClient.GetAsync(new Uri(destinationUrl));
            var content = respone.Content;

            if (respone.IsSuccessStatusCode && content is not null)
            {
                return content.ReadAsStream();
            }

            return Stream.Null;
        }

        public async Task<HttpResponseMessage> UploadFile(string destinationUrl, Stream content, string fileName)
        {
            using var multipartFormData = new MultipartFormDataContent
            {
                { new StreamContent(content), "file", fileName }
            };

            var respone = await _httpClient.PostAsync(new Uri(destinationUrl), multipartFormData);

            return respone;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
