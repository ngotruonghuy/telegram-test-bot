﻿using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using TelegramBotFit.Factory;
using TelegramBotFit.Interface;

namespace TelegramBotFit.Implement
{
    public class Handler : IHandler
    {
        public Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException
                    => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);

            return Task.CompletedTask;
        }

        public async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update.Message is not {} message)
                return;

            if ((message.LeftChatMember != null) || (message.NewChatMembers != null))
                return;

            IMessageHandler messageHandler;

            if ((message?.Text== "/convert")||(message?.Caption == "/convert"))
            {
                messageHandler = MessageHandlerFactory.GetInstance("ConvertFunction");
            }
            else if ((message?.Text == "/cat") || (message?.Text == "/meow")) 
            {
                messageHandler = MessageHandlerFactory.GetInstance("GetCat");
            }
            else
            {
                messageHandler = MessageHandlerFactory.GetInstance("NotSupportedMessage");
            }

            await messageHandler.HandleMessagge(botClient, message ?? new Message(), cancellationToken);
        }
    }
}
