﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelegramBotFit.Model.Commands;

namespace TelegramBotFit.Implement.CommandHandler
{
    public static class PythonHandler
    {
        public static Task Handle(PythonCommand command)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = command.AppRun;
            start.Arguments = string.Join(" ", command.Args);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    return Task.CompletedTask;
                }
            }
        }
    }
}
