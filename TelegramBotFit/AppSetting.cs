﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBotFit
{
    public static class AppSetting
    {
        public static string BotToken => ConfigurationManager.AppSettings.Get("BotToken") ?? string.Empty;
        public static string CatApi => ConfigurationManager.AppSettings.Get("CatAPI") ?? string.Empty;
        public static string ConvertAppDir => ConfigurationManager.AppSettings.Get("ConvertAppDir") ?? string.Empty;
        public static string ImageExt => ConfigurationManager.AppSettings.Get("ImageExt") ?? string.Empty;
        public static string CompleteStr => ConfigurationManager.AppSettings.Get("CompleteStr") ?? string.Empty;
     }
}
