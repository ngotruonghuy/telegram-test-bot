﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBotFit
{
    public static class FileHelper
    {
        public static void CreateDirectory(string path) => Directory.CreateDirectory(path);
        public static void EmptyDirectory(string path)
        {
            var dir = new DirectoryInfo(path);

            foreach (FileInfo file in dir.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo child in dir.GetDirectories())
            {
                child.Delete(true);
            }
        }
    }
}
