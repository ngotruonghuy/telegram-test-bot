﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBotFit.Interface
{
    public interface IHttpClient
    {
        Task<HttpResponseMessage> UploadFile(string destinationUrl, Stream content, string fileName);
        Task<Stream> DownloadFile(string destinationUrl);
    }
}
