﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelegramBotFit.Interface;

namespace TelegramBotFit.Factory
{
    public static class MessageHandlerFactory
    {
        private static Dictionary<string, IMessageHandler> messageHandlers = new Dictionary<string, IMessageHandler>();

        public static IMessageHandler GetInstance(string name)
        {
            if (messageHandlers.ContainsKey(name))
                return messageHandlers[name];
            throw new InvalidOperationException("Not Supported Handler");
        }

        public static void AddHandler(string name, IMessageHandler handler)
        {
            messageHandlers.Add(name, handler);
        }
    }
}
