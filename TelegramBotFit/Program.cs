﻿using System.Configuration;
using System.Net;
using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types.Enums;
using TelegramBotFit.Factory;
using TelegramBotFit.Implement;
using TelegramBotFit.Implement.CommandHandler;
using TelegramBotFit.Implement.MessageHandler;
using TelegramBotFit.Interface;
using TelegramBotFit.Model.Commands;

namespace TelegramBotFit
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var dispatcher = new CommandDispatcher();
            dispatcher.RegisterHandler<PythonCommand>(PythonHandler.Handle);

            IHandler handler = new Handler();
            MessageHandlerFactory.AddHandler("NotSupportedMessage", new NotSupportedFunction());
            MessageHandlerFactory.AddHandler("ConvertFunction", new ConvertToDiagramFunction(dispatcher));
            MessageHandlerFactory.AddHandler("GetCat", new GetCat());

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var botClient = new TelegramBotClient(AppSetting.BotToken);
            using CancellationTokenSource cts = new();

            ReceiverOptions receiverOptions = new()
            {
                AllowedUpdates = Array.Empty<UpdateType>()
            };

            botClient.StartReceiving(
                updateHandler: handler.HandleUpdateAsync,
                pollingErrorHandler: handler.HandlePollingErrorAsync,
                receiverOptions: receiverOptions,
                cancellationToken: cts.Token
            );

            var me = await botClient.GetMeAsync();
            Console.WriteLine($"Start listening for @{me.Username} (type q to stop)");
            
            string? escapeCharacter;
            do
            {
                escapeCharacter = Console.ReadLine();
            } while (escapeCharacter != "q");

            cts.Cancel();
        }
    }
}