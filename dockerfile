FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /App

# Copy everything
COPY . ./
# Restore as distinct layers
RUN dotnet restore
# Build and publish a release
RUN dotnet publish -c Release -o out

# Build runtime image
FROM ngotruonghuy/mix-runtime:net6-py39-1
WORKDIR /App
COPY --from=build-env /App/out .
COPY src ./pythonApp
ENTRYPOINT ["dotnet", "TelegramBotFit.dll"]